import { jest } from '@jest/globals';
import { NativeModules } from 'react-native';

jest.mock('react-native-localize', () => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const RNLocalize = require('../../__mocks__/mock-react-native-localize');
  RNLocalize.getLocales();
  NativeModules.RNLocalize = RNLocalize;
  return RNLocalize;
});