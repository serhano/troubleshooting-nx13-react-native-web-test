/* eslint-disable global-require */
import i18n from 'i18n-js'
import * as RNLocalize from 'react-native-localize'
import memoizeOne from 'memoize-one'
import { I18nManager } from 'react-native'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const translationGetters: any = {
    // lazy requires (metro bundler does not support symlinks)
    'en-US': (): JSON => require('./locales/en.json'),
    'de-DE': (): JSON => require('./locales/de.json'),
    'tr-TR': (): JSON => require('./locales/tr.json'),
}

export const translate = memoizeOne((key, config?) => i18n.t(key, config))

export const setI18nConfig = (): void => {
    // fallback if no available language fits
    const fallback = { languageTag: 'en-US', isRTL: false }

    const { languageTag, isRTL } =
        RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
        fallback

    // update layout direction
    I18nManager.forceRTL(isRTL)
    // set i18n-js config
    i18n.defaultLocale = 'en'
    i18n.locale = languageTag
    i18n.fallbacks = true
    i18n.translations = { [languageTag]: translationGetters[languageTag]() }
}

export const initI18n = async () => {
    await setI18nConfig()
}

export default i18n
